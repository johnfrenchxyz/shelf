# Shelf UI Prototyping

The purpose of this repository is to test some of the visual elements and front-end code that will be used in the Shelf project in the coming months.  The new official repository for Shelf will be its own group in GitLab, and will be controlled by more people than just myself.

For this codebase, I am creating a splash page for shelf to do testing and promotion. For more information on the project and the work I do in general, [please follow along on my development blog](https://blog.johnfrench.xyz).
